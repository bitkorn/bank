<?php

namespace Bitkorn\Bank;

return [
    'router' => [
        'routes' => [],
    ],
    'controllers' => [
        'factories' => [
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            // service
            Service\BankService::class => Factory\Service\BankServiceFactory::class,
            // fieldset
            Form\Fieldset\BankFieldset::class => Factory\Form\Fieldset\BankFieldsetFactory::class,
            // table
            Table\BankTable::class => Factory\Table\BankTableFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories' => [],
        'invokables' => [],
        'aliases' => [],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
