<?php

namespace Bitkorn\Bank\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;

abstract class AbstractBankTable extends AbstractLibTable
{
    protected array $bankColumns = ['bank_label', 'bank_institute', 'bank_holder', 'bank_iban', 'bank_bic', 'bank_code_bank', 'bank_code_holder', 'bank_time_create'];

    protected BankTable $bankTable;

    public function setBankTable(BankTable $bankTable): void
    {
        $this->bankTable = $bankTable;
    }
}
