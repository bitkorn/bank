<?php

namespace Bitkorn\Bank\Table;

use Bitkorn\Bank\Entity\BankEntity;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class BankTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'bank';

    /**
     * @param string $bankUuid
     * @return array
     */
    public function getBank(string $bankUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['bank_uuid' => $bankUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertBank(BankEntity $bankEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        $bankEntity->setPrimaryKeyValue($uuid);
        try {
            $insert->values($bankEntity->getStorage());
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $bankUuid
     * @return int
     */
    public function deleteBank(string $bankUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['bank_uuid' => $bankUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $bankUuid
     * @param BankEntity $bankEntity
     * @return int
     */
    public function updateBank(string $bankUuid, BankEntity $bankEntity): int
    {
        $update = $this->sql->update();
        $bankEntity->unsetPrimaryKey();
        try {
            $update->set($bankEntity->getStorage());
            $update->where(['bank_uuid' => $bankUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param Select $selectBankUuids A Select that only gives me bank_uuid's back.
     * @return array
     */
    public function getBanks(Select $selectBankUuids)
    {
        $select = $this->sql->select();
        try {
            $select->where->in('bank_uuid', $selectBankUuids);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
