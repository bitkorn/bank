<?php

namespace Bitkorn\Bank\Factory\Form\Fieldset;

use Bitkorn\Bank\Form\Fieldset\BankFieldset;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class BankFieldsetFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $fieldset = new BankFieldset();
        return $fieldset;
    }
}
