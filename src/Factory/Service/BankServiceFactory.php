<?php

namespace Bitkorn\Bank\Factory\Service;

use Bitkorn\Bank\Service\BankService;
use Bitkorn\Bank\Table\BankTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class BankServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new BankService();
        $service->setLogger($container->get('logger'));
        $service->setBankTable($container->get(BankTable::class));
        return $service;
    }
}
