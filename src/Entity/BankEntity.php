<?php

namespace Bitkorn\Bank\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class BankEntity extends AbstractEntity
{
    public array $mapping = [
        'bank_uuid' => 'bank_uuid',
        'bank_label' => 'bank_label',
        'bank_institute' => 'bank_institute',
        'bank_holder' => 'bank_holder',
        'bank_iban' => 'bank_iban',
        'bank_bic' => 'bank_bic',
        'bank_code_bank' => 'bank_code_bank',
        'bank_code_holder' => 'bank_code_holder',
        'bank_time_create' => 'bank_time_create',
    ];

    protected $primaryKey = 'bank_uuid';

    protected $dbUnusedFields = ['bank_time_create'];

    /**
     * @return string
     */
    public function getBankUuid()
    {
        if (!isset($this->storage['bank_uuid'])) {
            return '';
        }
        return $this->storage['bank_uuid'];
    }

    /**
     * @return string
     */
    public function getBankLabel()
    {
        if (!isset($this->storage['bank_label'])) {
            return '';
        }
        return $this->storage['bank_label'];
    }

    /**
     * @return string
     */
    public function getBankInstitute()
    {
        if (!isset($this->storage['bank_institute'])) {
            return '';
        }
        return $this->storage['bank_institute'];
    }

    /**
     * @return string
     */
    public function getBankHolder()
    {
        if (!isset($this->storage['bank_holder'])) {
            return '';
        }
        return $this->storage['bank_holder'];
    }

    /**
     * @return string
     */
    public function getBankIban()
    {
        if (!isset($this->storage['bank_iban'])) {
            return '';
        }
        return $this->storage['bank_iban'];
    }

    /**
     * @return string
     */
    public function getBankBic()
    {
        if (!isset($this->storage['bank_bic'])) {
            return '';
        }
        return $this->storage['bank_bic'];
    }

    /**
     * @return string
     */
    public function getBankCodeBank()
    {
        if (!isset($this->storage['bank_code_bank'])) {
            return '';
        }
        return $this->storage['bank_code_bank'];
    }

    /**
     * @return string
     */
    public function getBankCodeHolder()
    {
        if (!isset($this->storage['bank_code_holder'])) {
            return '';
        }
        return $this->storage['bank_code_holder'];
    }

    /**
     * @return string
     */
    public function getBankTimeCreate()
    {
        if (!isset($this->storage['bank_time_create'])) {
            return '';
        }
        return $this->storage['bank_time_create'];
    }
}
