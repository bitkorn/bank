<?php

namespace Bitkorn\Bank\Form\Fieldset;

use Bitkorn\Bank\Entity\BankEntity;
use Bitkorn\Trinket\Form\AbstractFieldset;
use Bitkorn\Trinket\Hydrator\EntityHydrator;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\I18n\Validator\Alnum;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class BankFieldset extends AbstractFieldset implements InputFilterProviderInterface
{

    public function init()
    {
        $this->setName('bank_fieldset');
        $this->setHydrator(new EntityHydrator());
        $this->setObject(new BankEntity());

        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'bank_uuid']);
        }
        $this->add(['name' => 'bank_label']);
        $this->add(['name' => 'bank_institute']);
        $this->add(['name' => 'bank_holder']);
        $this->add(['name' => 'bank_iban']);
        $this->add(['name' => 'bank_bic']);
        $this->add(['name' => 'bank_code_bank']);
        $this->add(['name' => 'bank_code_holder']);
    }

    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['bank_uuid'] = ['required' => true, 'validators' => [['name' => Uuid::class]]];
        }

        $filter['bank_label'] = [
            'required' => true,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['bank_institute'] = [
            'required' => true,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['bank_holder'] = [
            'required' => true,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['bank_iban'] = [
            'required' => false,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => Alnum::class,
                    'break_chain_on_failure' => true,
                ],
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 16,
                        'max' => 30,
                    ]
                ]
            ]
        ];

        $filter['bank_bic'] = [
            'required' => false,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => Alnum::class,
                    'break_chain_on_failure' => true,
                ],
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 8,
                        'max' => 11,
                    ]
                ]
            ]
        ];

        $filter['bank_code_bank'] = [
            'required' => false,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => Alnum::class,
                    'break_chain_on_failure' => true,
                ],
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 6,
                        'max' => 20,
                    ]
                ]
            ]
        ];

        $filter['bank_code_holder'] = [
            'required' => false,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => Alnum::class,
                    'break_chain_on_failure' => true,
                ],
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 6,
                        'max' => 20,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
