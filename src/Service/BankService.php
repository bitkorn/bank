<?php

namespace Bitkorn\Bank\Service;

use Bitkorn\Bank\Entity\BankEntity;
use Bitkorn\Bank\Table\BankTable;
use Bitkorn\Trinket\Service\AbstractService;

class BankService extends AbstractService
{
    /**
     * @var BankTable
     */
    protected $bankTable;

    /**
     * @param BankTable $bankTable
     */
    public function setBankTable(BankTable $bankTable): void
    {
        $this->bankTable = $bankTable;
    }

    public function insertBank(BankEntity $bankEntity): string
    {
        return $this->bankTable->insertBank($bankEntity);
    }

    public function deleteBank(string $bankUuid): bool
    {
        return $this->bankTable->deleteBank($bankUuid) >= 0;
    }

    public function updateBank(string $bankUuid, BankEntity $bankEntity): bool
    {
        return $this->bankTable->updateBank($bankUuid, $bankEntity) >= 0;
    }

}
