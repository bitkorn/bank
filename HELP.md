
[wikipedia.org/IBAN](https://de.wikipedia.org/wiki/Internationale_Bankkontonummer)

[wikipedia.org/BIC](https://de.wikipedia.org/wiki/ISO_9362)

[bundesbank.de/iban-regeln](https://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/iban-regeln)

[bundesbank.de/bankleitzahlen](https://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen)

[bundesbank.de/bankleitzahlen/download](https://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen/download-bankleitzahlen-602592)

[ecb.europa.eu/monthly_list-MID](https://www.ecb.europa.eu/stats/financial_corporations/list_of_financial_institutions/html/monthly_list-MID.en.html)
