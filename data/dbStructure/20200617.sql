create table bank
(
    bank_uuid        uuid                                not null
        constraint bank_pk
            primary key,
    bank_label       varchar(100)                        not null,
    bank_institute   varchar(200)                        not null,
    bank_holder      varchar(200)                        not null,
    bank_iban        varchar(34)                         not null,
    bank_bic         varchar(11)                         not null,
    bank_code_bank   varchar(20)                         not null,
    bank_code_holder varchar(20)                         not null,
    bank_time_create timestamp default CURRENT_TIMESTAMP not null
);

comment on column bank.bank_institute is 'Name der Bank';

comment on column bank.bank_holder is 'Kontoinhaber';

comment on column bank.bank_code_bank is 'Bankleitzahl (Teil der IBAN)';

comment on column bank.bank_code_holder is 'Kontonummer (Teil der IBAN)';

alter table bank
    owner to postgres;

