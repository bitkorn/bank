create table public.bank
(
    bank_uuid      uuid         not null
        constraint bank_pk
            primary key,
    bank_label     varchar(100) not null,
    bank_institute varchar(200) not null,
    bank_holder    varchar(200) not null,
    bank_iban      varchar(34)  not null,
    bank_bic       varchar(11)  not null
);

comment on column public.bank.bank_institute is 'Name der Bank';

comment on column public.bank.bank_holder is 'Kontoinhaber';

alter table public.bank
    owner to postgres;

